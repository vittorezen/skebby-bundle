<?php

namespace Zen\Bundle\SkebbyBundle\EventListener;

use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;

class CleanNumberListener
{
    private $regExp;

    /**
     * @param string $regExp Remove all chars ($regExp) from the number
     */
    public function __construct($regExp)
    {
        $this->regExp = $regExp;
    }

    /**
     * Set RegExp.
     *
     * @param strinf $regExp
     *
     * @return \Zen\Bundle\SkebbyBundle\EventListener\CleanNumberListener
     */
    public function setRegExp($regExp)
    {
        $this->regExp = $regExp;

        return $this;
    }

    /**
     * Get RegExp.
     *
     * @return string
     */
    public function getRegExp()
    {
        return $this->regExp;
    }

    /**
     * Clean number using RegExp.
     *
     * @param \Zen\Bundle\SkebbyBundle\Event\PreSendEvent $event
     */
    public function cleanNumber(PreSendEvent $event)
    {
        $phones = $event->getPhones();

        $fn = function (&$phone, $key, $regexp) {
            $phone = preg_replace($regexp, '', $phone);
        };

        array_walk($phones, $fn, $this->regExp);

        $event->setPhones($phones);
    }
}
