<?php

namespace Zen\Bundle\SkebbyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PreSendEvent extends Event
{
    private $phones;

    /**
     * @param array $phones
     */
    public function __construct($message, $phones)
    {
        $this->setMessage($message);
        $this->setPhones($phones);
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param array $phones
     *
     * @return \Zen\Bundle\SkebbyBundle\Event\PreSendEvent
     */
    public function setPhones($phones)
    {
        $this->phones = (is_array($phones) ? $phones : array($phones));

        return $this;
    }
}
