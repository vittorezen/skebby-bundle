<?php

namespace Zen\Bundle\SkebbyBundle\Lib;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;
use Zen\Bundle\SkebbyBundle\SkebbyEvents;
use Zen\Bundle\SkebbyBundle\Util\Skebby;

/**
 * Skebby manager class.
 */
class SkebbyManager
{
    /**
     * @var Skebby
     */
    private $skebby;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $dataCollector;

    /**
     * Constructor.
     *
     * @param Skebby                   $skebby
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(Skebby $skebby, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger)
    {
        $this->skebby = $skebby;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    /**
     * Send SMS.
     *
     * @param array  $recipients
     * @param string $text
     * @param int    $sms_type
     * @param string $sender_number
     * @param string $sender_string
     * @param string $user_reference
     * @param string $charset
     * @param array  $optional_headers
     */
    public function sendSMS(array $recipients, $text, $sms_type = Skebby::SMS_TYPE_CLASSIC, $sender_number = '', $sender_string = '', $user_reference = '')
    {
        $this->logger->debug('Skebby: Prepare send sms "'.$text.'" to phones '.implode(', ', $recipients));

        $event = new PreSendEvent($text, $recipients);
        $this->eventDispatcher->dispatch(SkebbyEvents::PRE_SEND, $event);
        $message = $event->getMessage();
        $phones = array_values($event->getPhones());

        $this->logger->debug('Skebby: Now send sms "' & $message & '" to phones '.implode(',', $phones));

        $result = $this->skebby->sendSMS($phones, $message, $sms_type, $sender_number, $sender_string, $user_reference);

        $dataCollector = [
            'datetime' => new \DateTime(),
            'sender_string' => $sender_string,
            'sender_number' => $sender_number,
            'message' => $message,
            'type' => $sms_type,
            'phones' => $phones,
            'response' => $result,
            'error' => null,
        ];

        if ($this->isResultError($result)) {
            $dataCollector['error'] = $this->getResultErrorMessage($result);
            $this->logger->error('Skebby: Send error: '.$result['raw_body_response']);
        } else {
            $this->logger->debug('Skebby: Send error: '.$result['raw_body_response']);
        }

        $this->dataCollector[] = $dataCollector;

        return $result;
    }

    /**
     * Get credit.
     *
     * @param string $charset
     *
     * @return array
     */
    public function getCredit()
    {
        $credit = $this->skebby->getCredit();
        $this->dataCollector[] = [
            'type' => 'get_credit',
            'datetime' => new \DateTime(),
            'sender_string' => '',
            'sender_number' => '',
            'message' => '',
            'phones' => '',
            'response' => $credit,
            'error' => null,
        ];

        return $credit;
    }

    /**
     * Check if result is an error.
     *
     * @param array $result
     *
     * @return bool
     */
    public function isResultError(array $result)
    {
        return $this->skebby->isResultError($result);
    }

    /**
     * Get result error message.
     *
     * @param array $result
     *
     * @return string
     */
    public function getResultErrorMessage(array $result)
    {
        return $this->skebby->getResultErrorMessage($result);
    }

    /**
     * Get error message.
     *
     * @param string $error
     *
     * @return string
     */
    public function getErrorMessage($error)
    {
        return $this->skebby->getErrorMessage($error);
    }

    public function getDataCollector()
    {
        return $this->dataCollector;
    }
}
