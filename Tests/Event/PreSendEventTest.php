<?php

namespace Zen\Bundle\SkebbyBundle\Tests\Event;

use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;

class PreSendEventTest extends \PHPUnit_Framework_TestCase
{
    public function testEvent()
    {
        $event = new PreSendEvent('message', array('1234'));

        $this->assertEquals('message', $event->getMessage());

        $this->assertCount(1, $event->getPhones());
        $event->setPhones(array('2345', '3456'));
        $this->assertCount(2, $event->getPhones());

        $phones = $event->getPhones();
        $firstPhone = $phones[0];
        $this->assertEquals('2345', $firstPhone);
    }
}
