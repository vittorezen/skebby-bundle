<?php

namespace Zen\Bundle\SkebbyBundle\Tests\EventListener;

use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;
use Zen\Bundle\SkebbyBundle\EventListener\CleanNumberListener;

class CleanNumberListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testCleanNumber()
    {
        $cleanNumber = new CleanNumberListener('/[^0-9]/');

        $phones = array('1234', '2345', '443456');

        $event = new PreSendEvent('message', $phones);
        $cleanNumber->cleanNumber($event);
        $elabPhones = $event->getPhones();

        foreach ($phones as $key => $value) {
            $this->assertEquals($value, $elabPhones[$key]);
        }

        $phones = array('1234abcudpe');
        $cleanNumber->cleanNumber($event);
        $elabPhones = $event->getPhones();

        $this->assertEquals('1234', $elabPhones[0]);
    }
}
