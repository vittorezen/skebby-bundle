<?php

namespace Zen\Bundle\SkebbyBundle\Tests\EventListener;

use Zen\Bundle\SkebbyBundle\Event\PreSendEvent;
use Zen\Bundle\SkebbyBundle\EventListener\AddPrefixListener;

class AddPrefixListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testAddPrefix()
    {
        $addPrefixListener = new AddPrefixListener('39');

        $this->assertEquals('39', $addPrefixListener->getPrefix());
        $addPrefixListener->setPrefix('44');
        $this->assertEquals('44', $addPrefixListener->getPrefix());

        $phones = array('1234', '2345', '443456');

        $event = new PreSendEvent('message', $phones);
        $addPrefixListener->addPrefix($event);
        $elabPhones = $event->getPhones();

        foreach ($phones as $key => $value) {
            $this->assertEquals('44'.$value, $elabPhones[$key]);
        }
    }
}
