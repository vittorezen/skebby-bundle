<?php

namespace Zen\Bundle\SkebbyBundle\Test\Util;

use Guzzle\Http\Exception\ServerErrorResponseException;
use Zen\Bundle\SkebbyBundle\Util\Skebby;

/**
 * Skebby test.
 */
class SkebbyTest extends \PHPUnit_Framework_TestCase
{
    protected $skebby;
    protected $httpClient;

    public function setUp()
    {
        $this->httpClient = $this->getMock('Guzzle\Http\ClientInterface');
        $this->skebby = new Skebby($this->httpClient, 'dummy', 'foobar');
    }

    public function testGetCredit()
    {
        $request = $this->getMock('Guzzle\Http\Message\RequestInterface');
        $response = $this->getMockBuilder('Guzzle\Http\Message\Response')->disableOriginalConstructor()->getMock();
        $this->httpClient->expects($this->once())->method('post')->will($this->returnValue($request));
        $request->expects($this->once())->method('send')->will($this->returnValue($response));
        $response->expects($this->once())->method('getBody')->will($this->returnValue('response=foo&bar=baz'));
        $this->skebby->getCredit();
    }

    public function testSendSMS()
    {
        $request = $this->getMock('Guzzle\Http\Message\RequestInterface');
        $response = $this->getMockBuilder('Guzzle\Http\Message\Response')->disableOriginalConstructor()->getMock();
        $this->httpClient->expects($this->once())->method('post')->will($this->returnValue($request));
        $request->expects($this->once())->method('send')->will($this->returnValue($response));
        $response->expects($this->once())->method('getBody')->will($this->returnValue('response=foo&bar=baz'));
        $this->skebby->sendSMS(array('rctp1'), 'message body...');
    }

    public function testHttpErrorWhileSendingSMS()
    {
        $this->httpClient->expects($this->once())->method('post')->will($this->throwException(new ServerErrorResponseException('bazinga!')));
        $result = $this->skebby->sendSMS(array('rctp1'), 'message body...');
        $this->assertEquals('error', $result['status']);
        $this->assertEquals('99', $result['code']);
        $this->assertEquals('bazinga!', $result['message']);
    }

    public function testGetErrorMessage()
    {
        $this->assertEquals('Errore sconosciuto', $this->skebby->getErrorMessage('foobar'));
        $this->assertEquals('Errore generico', $this->skebby->getErrorMessage('10'));
    }

    public function testGetResultErrorMessage()
    {
        $result = array();
        $this->assertEquals('', $this->skebby->getResultErrorMessage($result));
        $result = array('code' => '11');
        $this->assertEquals('Charset non valido', $this->skebby->getResultErrorMessage($result));
    }

    public function testisResultError()
    {
        $result = array('status' => 'wrong');
        $this->assertTrue($this->skebby->isResultError($result));
        $result = array('status' => 'success');
        $this->assertFalse($this->skebby->isResultError($result));
    }
}
