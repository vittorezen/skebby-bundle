<?php

namespace Zen\Bundle\SkebbyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SkebbyExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $container->setParameter('skebby.url', $config['url']);
        $container->setParameter('skebby.username', $config['username']);
        $container->setParameter('skebby.password', $config['password']);
        $container->setParameter('skebby.test_mode', $config['test_mode']);

        if (isset($config['clean_regexp'])) {
            $container->setParameter('skebby.clean_regexp', $config['clean_regexp']);
            $loader->load('EventListener/CleanNumberListener.xml');
        }
        if (isset($config['add_prefix'])) {
            $container->setParameter('skebby.add_prefix', $config['add_prefix']);
            $loader->load('EventListener/AddPrefixListener.xml');
        }
    }
}
