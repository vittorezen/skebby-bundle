<?php

namespace Zen\Bundle\SkebbyBundle\Util;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Skebby main class.
 */
class Skebby
{
    const NET_ERROR = 'Errore+di+rete+impossibile+spedire+il+messaggio';
    const SENDER_ERROR = 'Puoi+specificare+solo+un+tipo+di+mittente%2C+numerico+o+alfanumerico';
    const SMS_TYPE_CLASSIC = 'classic';
    const SMS_TYPE_CLASSIC_PLUS = 'classic_plus';
    const SMS_TYPE_BASIC = 'basic';

    private $httpClient;
    private $username;
    private $password;
    private $testMode;

    /**
     * Constructor.
     *
     * @param Client $httpClient
     * @param string $username
     * @param string $password
     * @param bool   $testMode
     */
    public function __construct(Client $httpClient, $username, $password, $testMode = false)
    {
        $this->httpClient = $httpClient;
        $this->username = $username;
        $this->password = $password;
        $this->testMode = $testMode;
    }

    /**
     * Send SMS.
     *
     * @param array  $recipients
     * @param string $text
     * @param int    $sms_type
     * @param string $sender_number
     * @param string $sender_string
     * @param string $user_reference
     * @param string $charset
     */
    public function sendSMS(array $recipients, $text, $sms_type = self::SMS_TYPE_CLASSIC, $sender_number = '', $sender_string = '', $user_reference = '', $charset = '')
    {
        switch ($sms_type) {
            case self::SMS_TYPE_CLASSIC:
                $method = 'send_sms_classic';
                break;
            case self::SMS_TYPE_CLASSIC_PLUS:
                $method = 'send_sms_classic_report';
                break;
            case self::SMS_TYPE_BASIC:
                $method = 'send_sms_basic';
                break;
            default:
                throw new \UnexpectedValueException(sprintf('Invalid SMS type: %s', $sms_type));
        }

        if ($this->testMode) {
            $method = 'test_'.$method;
        }

        $parameters = [
            'method' => $method,
            'text' => $text,
            'recipients' => [],
        ];

        foreach ($recipients as $recipient) {
            $parameters['recipients'][]['recipient'] = $recipient;
        }

        if ('' != $sender_number && '' != $sender_string) {
            return [
                'status' => 'failed',
                'message' => self::SENDER_ERROR,
            ];
        }
        if ('' != $sender_number) {
            $parameters['sender_number'] = $sender_number;
        }
        if ('' != $sender_string) {
            $parameters['sender_string'] = $sender_string;
        }
        if ('' != $user_reference) {
            $parameters['user_reference'] = $user_reference;
        }

        switch ($charset) {
            case 'UTF-8':
                $parameters['charset'] = 'UTF-8';
                break;
            case '':
            case 'ISO-8859-1':
            default:
        }

        return $this->doPostRequest($parameters);
    }

    /**
     * Get credit.
     *
     * @return array
     */
    public function getCredit()
    {
        return $this->doPostRequest([
            'method' => 'get_credit',
            'charset' => 'UTF-8',
        ]);
    }

    /**
     * Check if result is an error.
     *
     * @param array $result
     *
     * @return bool
     */
    public function isResultError(array $result)
    {
        return 'success' != $result['status'];
    }

    /**
     * Get result error message.
     *
     * @param array $result
     *
     * @return string
     */
    public function getResultErrorMessage(array $result)
    {
        return isset($result['code']) ? '[err:'.$result['code'].'] '.$result['message'] : '';
    }

    /**
     * Do POST request.
     *
     * @param array $data
     *
     * @return array
     */
    private function doPostRequest(array $data)
    {
        $data['username'] = $this->username;
        $data['password'] = $this->password;

        try {
            $response = $this->httpClient->request('post', null, [
                'form_params' => $data,
            ]);
        } catch (GuzzleException $e) {
            return ['status' => 'error', 'code' => 99, 'message' => $e->getMessage()];
        }

        return $this->parseQueryString($response->getBody());
    }

    /**
     * Parse query string returned by Skebby webservice.
     *
     * @param string $str
     *
     * @return array
     */
    private function parseQueryString($str)
    {
        $op = [
            'raw_body_response' => urldecode($str),
        ];
        $pairs = explode('&', $str);
        foreach ($pairs as $pair) {
            list($k, $v) = array_map('urldecode', explode('=', $pair));
            $op[$k] = $v;
        }

        return $op;
    }
}
