<?php

namespace Zen\Bundle\SkebbyBundle;

final class SkebbyEvents
{
    /**
     * L'evento skebby.pre_send è lanciato ogni volta prima dell'invio degli sms.
     *
     * L'ascoltatore dell'evento riceve un'istanza di
     * Zen\Bundle\SkebbyBundle\Event\FilterOrderEvent.
     *
     * @var string
     */
    const PRE_SEND = 'skebby.pre_send';
}
