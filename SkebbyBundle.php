<?php

namespace Zen\Bundle\SkebbyBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SkebbyBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new RegisterListenersPass('skebby.skebby_event_dispatcher', 'skebby.event_listener', 'skebby.event_subscriber'));
    }
}
